package edu.ntnu.idatt2001;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeckOfCardsTest {

  /**
   * The object to be tested.
   */
  private DeckOfCards deckOfCards ;

  /**
   * Sets up the test fixture.
   * (Called before every test case method.)
   */
  @BeforeEach
  void setUp() {
    deckOfCards = new DeckOfCards();
  }

  /**
   * Test that the deck is not empty.
   */
  @Test
  @DisplayName("fillDeckOfCards() should fill the deck with 52 cards and not be empty")
  void fillDeckOfCardsTest() {
    assertFalse(deckOfCards.getDeck().isEmpty());
  }

  @Test
  @DisplayName("deckSize() should return 52 cards")
  void deckSizeTest() {
    assertEquals(52, deckOfCards.getDeckSize());
  }

  /**
   * Test that the deck returns 52 cards.
   */
  @Test
  @DisplayName("getDeck() should return 52 cards")
  void getDeckTest() {
    assertEquals(52, deckOfCards.getDeck().size());
  }

  /**
   * Test that the deck returns the correct number of cards.
   */
  @Test
  @DisplayName("dealHand() should return correct number of cards")
  void dealHand() {
    int handSize = 5;
    assertEquals(handSize, deckOfCards.dealHand(handSize).size());
  }

  /**
   * Test that the deck throws an exception if the hand size is negative.
   */
  @Test
  @DisplayName("dealHand() should throw an exception if hand size is negative")
  void dealHandShouldThrowAnExceptionIfHandSizeIsNegative() {
    int handSize = -1;
    assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(handSize));
  }
}