package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {

  private char suit;
  private int face;
  private PlayingCard playingCard;

  @Test
  void constructorTest() {
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('A', 1));
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('S', 14));
  }

  @Test
  void getAsString() {
    playingCard = new PlayingCard('S', 1);
    assertEquals("S1", playingCard.getAsString());
  }

  @Test
  void getSuit() {
    playingCard = new PlayingCard('S', 1);
    assertEquals('S', playingCard.getSuit());
  }

  @Test
  void getFace() {
    playingCard = new PlayingCard('S', 1);
    assertEquals(1, playingCard.getFace());
  }
}