package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

  private HandOfCards handOfCards;
  private final DeckOfCards deckOfCards = new DeckOfCards();

  @Test
  void constructorTest() {
    assertThrows(IllegalArgumentException.class, () -> new HandOfCards(deckOfCards.dealHand(0)));
    assertThrows(IllegalArgumentException.class, () -> new HandOfCards(null));
  }

  @Test
  void getHandOfCards() {
    handOfCards = new HandOfCards(deckOfCards.dealHand(5));
    assertEquals(5, handOfCards.getHandOfCards().size());
  }

  @Test
  void getHandWithCards() {
    PlayingCard p1 = new PlayingCard('S',4);
    PlayingCard p2 = new PlayingCard('D',7);
    PlayingCard p3 = new PlayingCard('H',1);
    PlayingCard p4 = new PlayingCard('H',7);
    PlayingCard p5 = new PlayingCard('C',12);
    handOfCards = new HandOfCards(new ArrayList<PlayingCard>(Arrays.asList(p1,p2,p3,p4,p5)));
    assertEquals("S4, D7, H1, H7, C12, ", handOfCards.getHandWithCards(handOfCards.getHandOfCards()));
  }

  @Test
  void checkFlushNoFlush() {
    PlayingCard p1 = new PlayingCard('S',4);
    PlayingCard p2 = new PlayingCard('D',7);
    PlayingCard p3 = new PlayingCard('H',1);
    PlayingCard p4 = new PlayingCard('H',7);
    PlayingCard p5 = new PlayingCard('C',12);
    handOfCards = new HandOfCards(new ArrayList<PlayingCard>(Arrays.asList(p1,p2,p3,p4,p5)));
    assertEquals("No flush", handOfCards.checkFlush());
  }

  @Test
  void checkFlushFlushOfSpades() {
    PlayingCard p1 = new PlayingCard('S',4);
    PlayingCard p2 = new PlayingCard('S',7);
    PlayingCard p3 = new PlayingCard('S',1);
    PlayingCard p4 = new PlayingCard('S',7);
    PlayingCard p5 = new PlayingCard('S',12);
    handOfCards = new HandOfCards(new ArrayList<PlayingCard>(Arrays.asList(p1,p2,p3,p4,p5)));
    assertEquals("Flush of Spades", handOfCards.checkFlush());
  }
}