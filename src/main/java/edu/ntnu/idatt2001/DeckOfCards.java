package edu.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a deck of cards.
 * A deck of cards is a collection of 52 playing cards.
 * Provides methods to deal a hand of cards.
 * The deck of cards is shuffled when it is created.
 *
 * @author Ina Martini
 * @version 2020-01-10
 */
public class DeckOfCards {

  /**
   * An array of characters representing the four suits of a deck of cards.
   */
  private final char[] suit = {'S', 'H', 'D', 'C'};

  /**
   * An ArrayList of PlayingCard objects representing the deck of cards.
   */
  private ArrayList<PlayingCard> deck;


  /**
   * Creates an instance of a DeckOfCards.
   * The deck of cards is filled with 52 cards.
   * The deck of cards is shuffled.
   */
  public DeckOfCards() {
    this.deck = new ArrayList<>();
    fillDeckOfCards();
  }

  /**
   * Fills the deck of cards with 52 cards.
   * The deck of cards is filled with 13 cards of each suit.
   */
  public void fillDeckOfCards() {
    for (char suits : suit) {
      for (int i = 1; i < 14; i++) {
        deck.add(new PlayingCard(suits, i));
      }
    }
  }

  /**
   * Returns the deck of cards.
   *
   * @return the deck of cards as an ArrayList
   */
  public ArrayList<PlayingCard> getDeck() {
    return deck;
  }

  /**
   * Returns the number of cards in the deck of cards.
   *
   * @return the number of cards in the deck of cards as int
   */
  public int getDeckSize() {
    return deck.size();
  }

  /**
   * Shuffles the deck of cards.
   * The deck of cards is shuffled by randomly selecting a card from the deck
   * and adding it to a new deck of cards.
   * The new deck of cards is then used as the deck of cards.
   *
   * @return the shuffled deck of cards as an ArrayList
   */
  public ArrayList<PlayingCard> dealHand(int n) {
    Random random = new Random();
    ArrayList<PlayingCard> hand = new ArrayList<>();

    if (n <= 0) {
      throw new IllegalArgumentException("Number of cards must be greater than 0");
    }

    for (int i = 0; i < n; i++) {
      int randomCard = random.nextInt(deck.size());
      hand.add(deck.get(randomCard));
      deck.remove(randomCard);
    }
    return hand;
  }
}
