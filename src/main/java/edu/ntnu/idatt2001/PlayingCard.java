package edu.ntnu.idatt2001;

/**
 * Represents a playing card. A playing card has a number (face) between
 * 1 and 13, where 1 is called an Ace, 11 = Knight, 12 = Queen and 13 = King.
 * The card can also be one of 4 suits: Spade, Heart, Diamonds and Clubs.
 *
 * @author Ina Martini
 * @version 2020-01-10
 */
public class PlayingCard {

  private final char suit;
  private final int face;

  /**
   * Creates an instance of a PlayingCard with a given suit and face.
   *
   * @param suit The suit of the card, as a single character. 'S' for Spades,
   *             'H' for Heart, 'D' for Diamonds and 'C' for clubs
   * @param face The face value of the card, an integer between 1 and 13
   */
  public PlayingCard(char suit, int face) {
    if (suit != 'S' && suit != 'H' && suit != 'D' && suit != 'C') {
      throw new IllegalArgumentException("Suit must be one of 'S', 'H', 'D' or 'C'");
    }
    if (face < 1 || face > 13) {
      throw new IllegalArgumentException("Face must be an integer between 1 and 13");
    }
    this.face = face;
    this.suit = suit;
  }

  /**
   * Returns the suit and face of the card as a string.
   *
   * @return the suit and face of the card as a string
   */
  public String getAsString() {
    return String.format("%s%s", suit, face);
  }

  /**
   * Returns the suit of the card
   *
   * @return the suit of the card as char
   */
  public char getSuit() {
    return suit;
  }

  /**
   * Returns the face of the card
   *
   * @return the face of the card as int
   */
  public int getFace() {
    return face;
  }
}

