package edu.ntnu.idatt2001;

import java.util.ArrayList;

public class HandOfCards {

  /**
   * The hand of cards.
   */
  private ArrayList<PlayingCard> handOfCards;

  /**
   * Creates an instance of a HandOfCards.
   * The hand of cards is filled with 5 cards.
   * The hand of cards is filled with 5 cards from the deck of cards.
   *
   * @param cardsOnHand the hand of cards as an ArrayList
   */
  public HandOfCards(ArrayList<PlayingCard> cardsOnHand) {
    if (cardsOnHand == null || cardsOnHand.isEmpty())  {
      throw new IllegalArgumentException("Hand of cards can't be null or empty");
    }
    this.handOfCards = new ArrayList<>();
    handOfCards.addAll(cardsOnHand);
  }

  /**
   * Returns the hand of cards.
   *
   * @return the hand of cards as an ArrayList
   */
  public ArrayList<PlayingCard> getHandOfCards() {
    return handOfCards;
  }

  /**
   * Returns the hand of cards as a String.
   *
   * @param handOfCards the hand of cards as an ArrayList
   * @return the hand of cards as a String
   */
  public String getHandWithCards(ArrayList<PlayingCard> handOfCards) {
    StringBuilder hand = new StringBuilder();
    for (PlayingCard card : handOfCards) {
      hand.append(card.getAsString()).append(", ");
    }
    return hand.toString();
  }

  /**
   * Method to check if the player has a flush.
   *
   * @return the flush as a String
   */
  public String checkFlush() {
    return (handOfCards.stream().filter(card -> card.getSuit() == 'S').count() >= 5) ? "Flush of Spades" :
           (handOfCards.stream().filter(card -> card.getSuit() == 'H').count() >= 5) ? "Flush of Hearts" :
           (handOfCards.stream().filter(card -> card.getSuit() == 'D').count() >= 5) ? "Flush of Diamonds" :
           (handOfCards.stream().filter(card -> card.getSuit() == 'C').count() >= 5) ? "Flush of Clubs" : "No flush";
  }
}